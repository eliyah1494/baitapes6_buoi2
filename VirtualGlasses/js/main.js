let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

let renderGlasses = () => {
  let contentHTML = "";
  for (let i = 0; i < dataGlasses.length; i++) {
    contentHTML += `<img src="${dataGlasses[i].src}" class= "col-sm-4 ${dataGlasses[i]}">`;
  }
  document.getElementById("vglassesList").innerHTML = contentHTML;
};
renderGlasses();

var element = document.getElementById("vglassesList");
element.classList.add("glassess");

var listGlasses = document.getElementsByClassName("glassess")[0];

var nodeListImg = listGlasses.getElementsByTagName("img");

// Tạo hàm thử kính
let chooseGlasses = () => {
  for (let i = 0; i < nodeListImg.length; i++) {
    nodeListImg[i].addEventListener("click", function () {
      // xoá kính hiện tại khi click chọn kính mới
      document.querySelector("#avatar img").remove();

      // thêm kính mới
      var img = document.createElement("img");
      img.src = dataGlasses[i].virtualImg;
      var src = document.getElementById("avatar");
      src.appendChild(img);

      // xoá div hiện tại
      document.querySelector("#glassesInfo div").remove();

      const para = document.createElement("div");
      para.innerHTML = `<h5>${dataGlasses[i].name} - ${dataGlasses[i].brand} (${dataGlasses[i].color}</h5>
      <span style="background-color: red;  padding: 2px 6px 2px 4px">$ ${dataGlasses[i].color}</span>
      <span style="color: green; font-weight: bold">Stocking</span>
      <p>${dataGlasses[i].description}</p>`;

      document.getElementById("glassesInfo").appendChild(para);
      document.getElementById("glassesInfo").style.display = "block";
    });
  }
};
chooseGlasses();
